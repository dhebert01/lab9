"use strict";

// Write it all in your DOMContentLoaded event.
function setup(e){
    const btnComment = document.getElementsByTagName("button")[0];
    const btn = document.getElementsByTagName("button")[1];
    function valid(e){
        const uname = document.getElementById("uname");
        const unameRX = /^[A-Z]/;
        if(!unameRX.test(uname.value) && uname.value.length >= 5){
            uname.setCustomValidity("Must start with Upper case letter!");
        }
        else{
            uname.setCustomValidity("");
        }
        
    }
    btnComment.addEventListener("click", valid)
}

document.addEventListener("DOMContentLoaded", setup);